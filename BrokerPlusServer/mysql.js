class DatabaseHandler {
	constructor() {
		this.pool = require('mysql').createPool(require('./config').database);
	}

	query(query, params) {
		return new Promise((resolve, reject) => {
			try {
				this.pool.getConnection((err, connection) => {
					if (err) throw err;
					const queryArgs = [];
					queryArgs.push(query);
					if (params) queryArgs.push(params);
					queryArgs.push((error, results, fields) => {
						connection.release();
						if (error) throw error;
						resolve(results);
					})
					connection.query(...queryArgs);
				});
			} catch(error) {
				reject(error);
			}
		})
	}
}

module.exports = new DatabaseHandler();