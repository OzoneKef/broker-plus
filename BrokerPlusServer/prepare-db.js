const db = require('./mysql')

(async () => {
	await db.query(`
		CREATE DATABASE brokerplus;
		USE brokerplus;

		SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
		SET AUTOCOMMIT = 0;
		START TRANSACTION;
		SET time_zone = "+00:00";
		
		/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
		/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
		/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
		/*!40101 SET NAMES utf8mb4 */;
		
		CREATE TABLE \`chat_messages\` (
		\`id\` int(10) UNSIGNED NOT NULL,
		\`author\` int(10) UNSIGNED NOT NULL,
		\`text\` text COLLATE utf8_bin NOT NULL,
		\`date\` datetime NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
		
		CREATE TABLE \`sessions\` (
		\`id\` int(10) NOT NULL,
		\`token\` varchar(64) COLLATE utf8_bin NOT NULL,
		\`lifetime\` datetime DEFAULT NULL,
		\`closed\` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
		\`userid\` int(10) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
		
		CREATE TABLE \`users\` (
		\`id\` int(10) UNSIGNED NOT NULL,
		\`email\` varchar(64) COLLATE utf8_bin NOT NULL,
		\`password\` varchar(64) COLLATE utf8_bin NOT NULL,
		\`verified\` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
		\`firstname\` varchar(64) COLLATE utf8_bin DEFAULT NULL,
		\`lastname\` varchar(64) COLLATE utf8_bin DEFAULT NULL,
		\`isadmin\` int(1) UNSIGNED NOT NULL DEFAULT '0'
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
		
		CREATE TABLE \`verifications\` (
		\`id\` int(10) NOT NULL,
		\`userid\` int(10) NOT NULL,
		\`type\` enum('email') COLLATE utf8_bin NOT NULL,
		\`code\` varchar(64) COLLATE utf8_bin NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
		
		ALTER TABLE \`chat_messages\`
		ADD PRIMARY KEY (\`id\`);
		
		ALTER TABLE \`sessions\`
		ADD PRIMARY KEY (\`id\`);
		
		ALTER TABLE \`users\`
		ADD PRIMARY KEY (\`id\`),
		ADD UNIQUE KEY \`email\` (\`email\`);
		
		ALTER TABLE \`verifications\`
		ADD PRIMARY KEY (\`id\`);
		
		ALTER TABLE \`chat_messages\`
		MODIFY \`id\` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
		
		ALTER TABLE \`sessions\`
		MODIFY \`id\` int(10) NOT NULL AUTO_INCREMENT;
		
		ALTER TABLE \`users\`
		MODIFY \`id\` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
		
		ALTER TABLE \`verifications\`
		MODIFY \`id\` int(10) NOT NULL AUTO_INCREMENT;
		COMMIT;
		
		/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
		/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
		/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
		
	`);
	process.exit(0);
})