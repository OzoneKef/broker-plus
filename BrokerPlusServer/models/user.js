const db = require('../mysql');
const Model = require('./model');

class User extends Model {
	async Init(properties, existing) {
		if (!existing) {
			const captions = [];
			const setters = [];
			const values = [];

			if (!properties.email || !properties.password) throw new Error('Missing mandatory fields');

			const existingUser = (await db.query('SELECT * FROM users WHERE email=?', [properties.email]))[0];
			if (existingUser) throw new Error('Email allready claimed');

			Object.keys(properties).forEach(prop => {
				captions.push(prop);
				if (prop==='password') setters.push('SHA2(?, 0)'); 
				else setters.push('?');
				values.push(properties[prop]);
			})

			await db.query(`INSERT INTO users(${captions.join(', ')}) VALUES(${setters.join(', ')})`, [...values])
			properties = (await db.query('SELECT * FROM users WHERE email=?', [properties.email]))[0];
		}

		delete properties.password;
		properties.sessions = [];
		Object.keys(properties).forEach(prop => this[prop]=properties[prop]);

		global.state.users.push(this);
		this.readypromise.resolve(this);
	}

	static async get({ email, phone, id }) {
		let properties = null;
		if (email) {
			properties = (await db.query('SELECT * FROM users WHERE email=?', [email]))[0]; 
		} else if (phone) {
			//TODO: database users phone column
		} else if (id) {
			properties = (await db.query('SELECT * FROM users WHERE id=?', [id]))[0];
		} else {
			throw new Error('Either email or phone must be specified')
		}
		if (!properties) return null;
		const user = global.state.users.find(user => user.id === properties.id);
		if (user) return user
		else return new User(properties, true);
	}

	async set(props) {
		const properties = Object.keys(props);
		const values = [];
		const setters = [];
		properties.forEach(prop => {
			values.push(props[prop]);
			this[prop] = props[prop];
			if (props === 'password') setters.push('??=SHA2(?, 0)');
			else setters.push('??=?');
		});
		await db.query(`UPDATE users SET ${setters.join(', ')} WHERE id=?`, [...properties, ...values, this.id]);

		return this;
	}

	async checkAuth(password) {
		const user = (await db.query('SELECT id FROM users WHERE id=? AND password=SHA2(?, 0)', [this.id, password]))[0];
		return !!user;
	}

	attachSession(session) {
		this.sessions.push(session);
		session.user = this;
	}

	detachSession(session) {
		this.sessions.splice(this.sessions.find(sess => sess.id === session.id), 1);
		if (!this.session.length) this.Delete();
	}

	async sendVerification() {
		const code = Math.floor(Math.random()*Math.pow(10, 4));
		await db.query('DELETE FROM verifications WHERE userid=? AND type=?', [this.id, 'email']);
		await db.query('INSERT INTO verifications(userid, type, code) VALUES(?, ?, ?)', [this.id, 'email', code]);
		return code;
	}

	async checkVerification(code) {
		const verification = await db.query('SELECT * FROM verifications WHERE userid=? AND type=? AND code=?', [this.id, 'email', code]);
		if (!!verification[0]) {
			await this.set({ verified: 1 });
			await db.query('DELETE FROM verifications WHERE userid=? AND type=?', [this.id, 'email']);
			return true;
		} else {
			return false;
		}
	}

	Delete() {
		global.users.splice(user => user.id === this.id, 1);
	}

	assembleJSON() {
		return {
			email: this.email,
			firstname: this.firstname,
			lastname: this.lastname,
			verified: this.verified,
			id: this.id,
			isadmin: this.isadmin,
		}
	}
}

module.exports = User;