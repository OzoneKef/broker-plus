const Model = require('./model');
const db = require('../mysql');

class Chat extends Model{
	async Init({ name }) {
		global.state.chats[name] = this;
		this.name = this;
		this.readypromise.resolve(this);
	}

	async getHistory() {
		return (await db.query('SELECT * FROM chat_messages ORDER BY date DESC LIMIT 10'))
	}

	detachMessage(id) {
		return db.query('DELETE FROM chat_messages WHERE id=?', [id])
	}

	async appendMessage({ text, author }) {
		const date = new Date();
		const msg = await db.query('INSERT INTO chat_messages (author, text, date) VALUES (?, ?, ?)', [author, text, date]);
		return { id: msg.insertId, text, author, date }
	}
}

module.exports = Chat;