const db = require('../mysql');
const Model = require('./model');
const User = require('../models/user');

class Session extends Model {
	async Init(properties, existing) {
		if (!existing) {
			if (!properties.userid) throw new Error('Userid is mandatory for new sessions');
			properties.token = Session.generateToken();
			let interlopingSession = true;
			while (!!interlopingSession) {
				interlopingSession = (await db.query('SELECT * FROM sessions WHERE token=?', [properties.token]))[0];
				if (interlopingSession) properties.token = Session.generateToken();
			}
			await db.query('UPDATE sessions SET closed=1 WHERE userid=?', [properties.userid]);
			await db.query('INSERT INTO sessions (token, lifetime, userid) VALUES (?, NOW(), ?)', [properties.token, properties.userid]);
		}

		if (properties.token) {
			const sessionData = (await db.query('SELECT * FROM sessions WHERE token=? ORDER BY id DESC', [properties.token]))[0];
			Object.keys(sessionData).forEach(prop => this[prop] = sessionData[prop]);
		} else {
			throw new Error('Cannot get any session because of empty token');
		}

		this.user = await User.get({ id: this.userid });
		global.state.sessions.push(this);
		this.readypromise.resolve(this);
	}

	static generateToken() {
		const symbols = '1234567890abcdef'.split('');
		let token = '';

		while (token.length < 64) {
			if (!((token.length+1) % 17)) {
				token += '-';
			} else {
				token += symbols[Math.floor(Math.random()*symbols.length)];		
			}
		}

		return token;
	}

	static async get(token) {
		const session = global.state.sessions.find(session => session.token == token);
		if (session) {
			return session
		} else {
			const dbSession = (await db.query('SELECT * FROM sessions WHERE token=?', [token]))[0];
			if (dbSession) {
				return new Session(dbSession, true);
			} else {
				return false
			}
		}
	}
}

module.exports = Session;