const nodemailer = require('nodemailer');
const Model = require('./model');
const config = require('../config');
const transporter = nodemailer.createTransport(config.email)

class Email extends Model {
	async Init({ to, subject, text, html }) {
		this.result = await transporter.sendMail({
			from: `"Broker Plus" <${config.email.auth.user}>`,
			to,
			subject,
			text,
			html,
		});

		this.readypromise.resolve(this);
	}
}

module.exports = Email;