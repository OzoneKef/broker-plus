class Model {
	constructor(...params) {
		this.readypromise = {};
		this.readypromise.promise = new Promise(resolve => this.readypromise.resolve = resolve)
		this.Init(...params);

		return this.readypromise.promise;
	}

	async Init() {
		this.readypromise.resolve(this);
	}

	isReady() {
		return this.readypromise.promise();
	}
}

module.exports = Model;