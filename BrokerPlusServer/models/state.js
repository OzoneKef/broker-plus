const db = require('../mysql');
const Model = require('./model');

class State extends Model {
	async Init() {
		this.sessions = [];
		this.users = [];
		this.chats = {};

		this.readypromise.resolve(this);
	}
}

module.exports = State;