module.exports = {
	server: {
		port: 9000,
		secure: false,
	},
	database: {
		connectionLimit: 10,
		host: 'localhost',
		user: 'root',
		password: '',
		database: 'brokerplus',
	},
	email: {
		host: 'smtp.yandex.ru',
		port: 465,
		secure: true,
		auth: {
			user: 'BrokerPlus.app@yandex.ru',
			pass: '123456789bp',
		}
	},
}