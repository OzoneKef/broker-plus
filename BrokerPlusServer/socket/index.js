const Session = require('../models/session');
const User = require('../models/user')
const Chat = require('../models/chat');

module.exports = async server => {
	const io = require('socket.io')(server);
	const globalChat = await new Chat('global');

	io.use(async (socket, next) => {
		const { query = {} } = socket.handshake;
		const { token } = query;
		if (!token) return next(new Error('unathorized'));
		const session = await Session.get(token);
		if (session) {
			next();
		} else {
		 	next(new Error('unathorized'));
		}
	});

	io.on('connection', async socket => {
		const { query = {} } = socket.handshake;
		const { token } = query;
		const session = await Session.get(token);
		console.log('New socket connection from userid ', session.user.id, socket.id);
		session.socket = socket;
		const messages = (await globalChat.getHistory()).map((message, index) => new Promise(async (resolve, reject) => {
			message.author = (await User.get({ id: message.author })).assembleJSON();
			resolve(messages[index] = message);
		}));
		await Promise.all(messages);
		socket.emit('CHAT:HISTORY', messages);
		socket.on('CHAT:SEND', async data => {
			const message = await globalChat.appendMessage({ text: data.text, author: session.user.id });
			message.author = (await User.get({ id: message.author })).assembleJSON();
			io.emit('CHAT:MESSAGE', message);
		});
		socket.on('CHAT:DELETE', async data => {
			if (!session.user.isadmin) throw new Error('You are not admin');
			await globalChat.detachMessage(data);
			io.emit('CHAT:ERASE', data);
		});

		socket.on('disconnect', () => {
			console.log('Disconnected: ', session.user.id, socket.id)
			session.socket = null;
		})
	})
}
