const express = require('express');
const router = express.Router();

const User = require('../models/user');
const Session = require('../models/session');
const Email = require('../models/email');

router.post('/login', async (req, res) => {
	try {
		const { email, password } = req.body;
		let user = await User.get({ email });
		if (user) {
			if (await user.checkAuth(password)) {
				const data = user.assembleJSON();
				const session = await new Session({ userid: user.id, password })
				user.attachSession(session);
				const token = session.token;
				res.status(200).json({ user: data, token });
 			} else {
				res.status(403).json({ error: 'Invalid password' });
			}
		} else {
			res.status(403).json({ error: 'No user with this email' });
		}
	} catch(error) {
		console.error(error);
		res.status(500).end();
	}
});

router.post('/register', async (req, res) => {
	try {
		const { email, password, firstname, lastname } = req.body;
		if (!email || !password) throw new Error('Email and password are mandatory fields');
		let user = await User.get({ email });
		if (user) {
			res.status(403).json({ error: 'Email allready taken' });
		} else {
			user = await new User({ email, password, firstname, lastname });
			const data = user.assembleJSON();
			const session = await new Session({ userid: user.id, password })
			user.attachSession(session);
			const token = session.token;
			const code = await user.sendVerification();
			await new Email({
				to: user.email,
				subject: 'Подтверждение регистрации',
				html: `<body>Благодарим вас за регистрацию в нашем приложении. Ваш код для подтверждения почты: <b>${code}</b></body>`,
			});
			res.status(200).json({ user: data, token });
		}
	} catch(error) {
		console.error(error);
		res.status(500).end();
	}
});

router.post('/verify', async (req, res) => {
	try {
		const { code } = req.body;
		const { user } = req.session;
		if (!user) return res.status(403).json({ error: 'user not found' })
		if (await user.checkVerification(code)) {
			res.status(200).json({ user: user.assembleJSON() });
		} else {
			res.status(403).json({ error: 'invalid code' })
		}
	} catch(error) {
		console.error(error);
		res.status(500).end()
	}
})

router.get('/user', async (req, res) => {
	try {
		if (!req.session) return res.status(403).json({ error: 'user not found' })
		const { user } = req.session;
		res.status(200).json({ user: user.assembleJSON() });
	} catch(error) {
		console.error(error);
		res.status(500).end()
	}
})

module.exports = router;