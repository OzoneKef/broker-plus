const config = require('./config');

const fs = require('fs');
const express = require('express');
const app = express();
const server = config.server.secure ? require('https').createServer({
	key: fs.readFileSync(`${__dirname}/ssl/key.pem`),
	cert: fs.readFileSync(`${__dirname}/ssl/cert.pem`),
}, app) : require('http').createServer(app);
const bodyParser = require('body-parser');
const Session = require('./models/session');

(async () => {
	const State = require('./models/state');
	global.state = await new State();

	app.use(bodyParser.urlencoded({
		'extended': 'true'
	}));
	app.use(bodyParser.json());
	app.use(bodyParser.json({
		type: 'application/vnd.api+json'
	}));
	
	app.use(async (req, res, next) => {
		const token = req.header('X-Session-Key');
		if (token && token!=='null') {
			req.session = await Session.get(token);
		}
		next();
	});

	app.use('/resources', express.static(`${__dirname}/files`));
	app.use('/api/auth', require('./api/auth'));
	
	server.listen(config.server.port, () => {
		console.log(`listening on *:${config.server.port}`);
	});

	require('./socket')(server);
})()