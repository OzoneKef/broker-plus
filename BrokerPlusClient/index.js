import 'react-native-gesture-handler';
import { AppRegistry, YellowBox } from 'react-native';
import React from 'react';
import App from './App';
import { name as appName } from './app.json';
import { Provider } from 'react-redux';
import { Store, Persistor } from './src/reducers/store';
import { PersistGate } from 'redux-persist/integration/react'

YellowBox.ignoreWarnings(['Require cycle:']);

AppRegistry.registerComponent(appName, () => () => <>
	<Provider store={Store}>
		<PersistGate loading={null} persistor={Persistor}>
			<App/>
		</PersistGate>
	</Provider>
</>);
