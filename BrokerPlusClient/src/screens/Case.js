import React from 'react';
import {
	View,
	Text,
} from 'react-native';
import { connect } from 'react-redux';
import GlobalStyles from '../styles';

import Input from '../components/Input';
import Button from '../components/Button';

import CaseList from '../components/CaseList'

const FontStyle = { color: GlobalStyles.color.brightFont };

class CaseScreen extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
		}
	}

	render() {
		return <>
			<View style={{
				flex: 1,
				justifyContent: 'center',
				padding: 25,
				backgroundColor: GlobalStyles.color.screenBackgroundBright
			}}>
				<CaseList> </CaseList>
			</View>
		</>
	}
}

export default connect(
	state => ({
	}),
	{
	},
)(CaseScreen)