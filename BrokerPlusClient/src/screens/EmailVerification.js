import React from 'react';
import {
	View,
	Text,
} from 'react-native';
import { connect } from 'react-redux';
import GlobalStyles from '../styles';

import Input from '../components/Input';
import Button from '../components/Button';

const FontStyle = { color: GlobalStyles.color.brightFont };

class EmailScreen extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			code: null,
		}
	}

	render() {
		const { code } = this.state;
		const { User } = this.props;

		return <>
			<View style={{
				flex: 1,
				justifyContent: 'center',
				padding: 25,
				backgroundColor: GlobalStyles.color.screenBackground
			}}>	
				<View style={{ alignItems: 'center' }}>
					<Text style={{ ...FontStyle, textAlign: 'center', width: '75%', marginBottom: 10 }}>Пожалуйста, введите код, полученный по адресу</Text>
					<Text style={{ ...FontStyle, textAlign: 'center', marginBottom: 25 }}>{(User.CurrentUser||{}).email}</Text>
				</View>
				<View style={{ alignItems: 'center' }}>
					<Input
						onChange={code => {
							if (code && code.length > 4) return
							this.setState({ code })
						}}
						value={code}
						placeholder={'Код'}
						maxLength={4}
						theme={'dark'}
						keyboardType={'numeric'}
						textStyle={{
							fontSize: 40,
							textAlign: 'center',
						}}
						style={{
							marginBottom: 25,
							width: '50%',
						}}
					/>
					<Button
						loading={User.Fetch}
						disabled={(User.Fetch || !code || (code && code.length !== 4))}
						theme={'dark'}
						style={{ width: '50%' }}
						onPress={() => this.props.Verify({ code })}
					><Text>Проверить код</Text></Button>
				</View>
			</View>
		</>
	}
}

export default connect(
	state => ({
		User: state.User,
	}),
	{
		Verify: params => ({ type: 'VERIFY_ATTEMPT', params })
	},
)(EmailScreen)