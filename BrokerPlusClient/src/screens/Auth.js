import React from 'react';
import {
	View,
	Text,
} from 'react-native';
import { connect } from 'react-redux';
import GlobalStyles from '../styles';

import Input from '../components/Input';
import Button from '../components/Button';

const FontStyle = { color: GlobalStyles.color.brightFont };

class AuthScreen extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			email: null,
			password: null,
		}
	}

	componentDidMount() {
		if (this.props.User.Token) this.props.CheckToken();
	}

	render() {
		const { email, password } = this.state;
		const { User } = this.props;

		return <>
			<View style={{
				flex: 1,
				justifyContent: 'center',
				padding: 25,
				backgroundColor: GlobalStyles.color.screenBackground
			}}>
				<Input
					onChange={email => this.setState({ email })}
					value={email}
					keyboardType={'email-address'}
					placeholder={'Email'}
					theme={'dark'}
					maxLength={64}
					style={{
						marginBottom: 10,
					}}
				/>
				<Input
					onChange={password => this.setState({ password })}
					secure={true}
					value={password}
					placeholder={'Пароль'}
					theme={'dark'}
					maxLength={64}
					style={{
						marginBottom: 10,
					}}
				/>
				<Button
					loading={User.Fetch}
					disabled={(User.Fetch || !email || !password)}
					theme={'dark'}
					onPress={() => this.props.Login({ email, password })}
				><Text>Войти</Text></Button>
				<Text style={{ ...FontStyle, textAlign: 'center', marginTop: 20, ...GlobalStyles.linkStyle }} onPress={this.props.GoToRegister}>Регистрация</Text>
			</View>
		</>
	}
}

export default connect(
	state => ({
		User: state.User,
	}),
	{
		GoToRegister: () => ({ type: 'NAVIGATE', params: { route: 'Register', params: {} } }),
		Login: params => ({ type: 'LOGIN_ATTEMPT', params }),
		CheckToken: () => ({ type: 'CHECK_USER' }),
	},
)(AuthScreen)