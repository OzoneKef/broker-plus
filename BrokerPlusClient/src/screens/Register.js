import React from 'react';
import {
	View,
	Text,
} from 'react-native';
import { connect } from 'react-redux';
import GlobalStyles from '../styles';

import Input from '../components/Input';
import Button from '../components/Button';

const FontStyle = { color: GlobalStyles.color.brightFont };

class RegisterScreen extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			email: null,
			password: null,
			firstname: null,
			lastname: null,
		}
	}

	render() {
		const { email, password, firstname, lastname } = this.state;
		const { User } = this.props;

		return <>
			<View style={{
				flex: 1,
				justifyContent: 'center',
				padding: 25,
				backgroundColor: GlobalStyles.color.screenBackground
			}}>
				<View style={{ flexDirection: 'row' }}>
					<Input
						onChange={firstname => this.setState({ firstname })}
						value={firstname}
						placeholder={'Имя'}
						maxLength={64}
						theme={'dark'}
						style={{
							marginBottom: 10,
							flexBasis: '50%',
							flexShrink: 1,
							marginRight: 30,
						}}
					/>
					<Input
						onChange={lastname => this.setState({ lastname })}
						value={lastname}
						placeholder={'Фамилия'}
						maxLength={64}
						theme={'dark'}
						style={{
							marginBottom: 10,
							flexShrink: 1,
							flexBasis: '50%',
						}}
					/>
				</View>
				<Input
					onChange={email => this.setState({ email })}
					value={email}
					keyboardType={'email-address'}
					placeholder={'Email'}
					theme={'dark'}
					maxLength={64}
					style={{
						marginBottom: 10,
					}}
				/>
				<Input
					onChange={password => this.setState({ password })}
					secure={true}
					value={password}
					placeholder={'Пароль'}
					maxLength={64}
					theme={'dark'}
					style={{
						marginBottom: 10,
					}}
				/>
				<Button
					loading={User.Fetch}
					disabled={(User.Fetch || !email || !password || !firstname || !lastname)}
					theme={'dark'}
					onPress={() => this.props.Register({ email, password, firstname, lastname })}
				><Text>Зарегистрироваться</Text></Button>
				<Text style={{ ...FontStyle, textAlign: 'center', marginTop: 20 }}>Уже есть учетная запись? <Text style={GlobalStyles.linkStyle} onPress={this.props.GoToAuth}>Войти</Text></Text>
			</View>
		</>
	}
}

export default connect(
	state => ({
		User: state.User,
	}),
	{
		Register: params => ({ type: 'REGISTER', params }),
		GoToAuth: () => ({ type: 'NAVIGATE', params: { route: 'Auth', params: {} } })
	},
)(RegisterScreen)