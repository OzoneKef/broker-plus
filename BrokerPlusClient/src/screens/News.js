import React from 'react';
import {
	View,
	Text,
} from 'react-native';
import { connect } from 'react-redux';
import GlobalStyles from '../styles';

import Input from '../components/Input';
import Button from '../components/Button';
import News from '../components/News';

const FontStyle = { color: GlobalStyles.color.brightFont };

class NewsScreen extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
		}
	}

	render() {
		return <>
			<View style={{
				flex: 1,
				justifyContent: 'center',
				padding: 0,
				backgroundColor: GlobalStyles.color.screenBackgroundBright
			}}>
				<News></News>
			</View>
		</>
	}
}

export default connect(
	state => ({
	}),
	{
	},
)(NewsScreen)