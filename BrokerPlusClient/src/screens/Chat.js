import React from 'react';
import {
	View,
	Text,
} from 'react-native';
import { connect } from 'react-redux';
import GlobalStyles from '../styles';
import Chat from '../components/Chat';

import Input from '../components/Input';
import Button from '../components/Button';

const FontStyle = { color: GlobalStyles.color.brightFont };

class ChatScreen extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
		}
	}

	render() {
		return <>
			<View
			style={{
				flex: 1,
				backgroundColor: GlobalStyles.color.screenBackgroundBright,
			}}>
				<Chat room={'global'}/>
			</View>
		</>
	}
}

export default connect(
	state => ({
	}),
	{
	},
)(ChatScreen)