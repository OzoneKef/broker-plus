import {
	takeLatest,
} from 'redux-saga/effects';
import config from '../../config.json';
import { Store } from './store';
import { get, post } from './networking';

const initialState = {
	Token: null,
	CurrentUser: {},
	Verified: false,
	Fetch: false,
	Error: false,
}

export const UserReducer = (state = initialState, action) => {
	switch (action.type) {
		case 'REGISTER':
			return { ...state, Fetch: true, Error: false }
		case 'LOGIN_ATTEMPT':
			return { ...initialState, Fetch: true, Error: false }
		case 'LOGIN_FAIL':
			return { ...initialState, Error: action.params, Fetch: false }
		case 'VERIFY_FAIL':
			return { ...state, Error: action.params, Fetch: false };
		case 'CLEAN_LOGIN_ERROR':
			return { ...state, Error: false }
		case 'UPDATE_USER':
			return { ...state, CurrentUser: action.params.user, Token: action.params.token, Verified: action.params.verified, Fetch: false, Error: false }
		case 'LOGOUT':
			return { ...initialState }
		case 'persist/REHYDRATE': {
			if (!action.payload) return state
			return { ...action.payload.User, Fetch: false, Error: false }
		}
		default:
			return state;
	}
}

export function* UserActions() {
	yield takeLatest('LOGIN_ATTEMPT', async ({ params }) => {
		try {
			const res = await post({ url: config.server+'/api/auth/login', data: params });
			Store.dispatch({ type: 'UPDATE_USER', params: res.data });
			if (res.data.user.verified) {
				Store.dispatch({ type: 'NAVIGATE', params: { route: 'News', params: {} } });
				Store.dispatch({ type: 'CLEAN_HISTORY' });
				Store.dispatch({ type: 'SOCKET:CONNECT', params: res.data.token })
			} else {
				Store.dispatch({ type: 'NAVIGATE', params: { route: 'Email', params: {} } });
			}
		} catch (error) {
			Store.dispatch({ type: 'LOGIN_FAIL', params: error });
		}
	})

	yield takeLatest('REGISTER', async ({ params }) => {
		try {
			const res = await post({ url: config.server+'/api/auth/register', data: params });
			Store.dispatch({ type: 'UPDATE_USER', params: res.data });
			Store.dispatch({ type: 'NAVIGATE', params: { route: 'Email', params: {} } });
		} catch (error) {
			Store.dispatch({ type: 'LOGIN_FAIL', params: error });
		}
	})

	yield takeLatest('VERIFY_ATTEMPT', async ({ params }) => {
		try {
			const res = await post({ url: config.server+'/api/auth/verify', data: params });
			Store.dispatch({ type: 'UPDATE_USER', params: res.data });
			if (res.data.user.verified) {
				Store.dispatch({ type: 'NAVIGATE', params: { route: 'News', params: {} } });
				Store.dispatch({ type: 'CLEAN_HISTORY' });
				Store.dispatch({ type: 'SOCKET:CONNECT', params: res.token })
			}
		} catch (error) {
			Store.dispatch({ type: 'VERIFY_FAIL', params: error });
		}
	})

	yield takeLatest('CHECK_USER', async () => {
		const res = await get({ url: config.server+'/api/auth/user' });
		if (res.success) {
			Store.dispatch({ type: 'UPDATE_USER', params: res.data });
			if (res.data.user.verified) {
				Store.dispatch({ type: 'NAVIGATE', params: { route: 'News', params: {} } });
				Store.dispatch({ type: 'CLEAN_HISTORY' });
			} else {
				Store.dispatch({ type: 'NAVIGATE', params: { route: 'Email', params: {} } });
			}
		}
	})
}