import { Image } from "react-native";

const initialState = {
	Route: 'Auth',
	history: [],
	Params: null,
	Update: 0,
}

export const RoutingReducer = (state = initialState, action) => {
	switch (action.type) {
		case 'NAVIGATE':
			return { ...state, Route: action.params.route, Params: action.params.params, history: [ ...state.history, { Route: state.Route, Params: state.Params } ], Update: state.Update+1  }
		case 'HISTORY_POP':
			const history = state.history;
			if (history.length) {
				const newRoute = history.pop();
				return { ...state, Route: newRoute.Route, Params: newRoute.Params, Update: state.Update+1 }
			} else {
				return state
			}
		case 'CLEAN_HISTORY':
			return { ...state, history: [] }
		case 'LOGOUT':
			return { ...initialState }
		default:
			return state;
	}
}