const initialState = {
	global: [],
}

export const ChatReducer = (state = initialState, action) => {
	switch (action.type) {
		case 'CHAT:MESSAGES':
			console.log(action.params);
			return { ...state, global: [ ...action.params, ...state.global ] }
		case 'CHAT:ERASE':
			state.global = state.global.filter(item => item.id!==action.params);
			return { ...state }
		case 'LOGOUT':
		case 'CHAT:FLUSH':
			return initialState;
		default:
			return state;
	}
}