import { createStore, combineReducers, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import AsyncStorage from '@react-native-community/async-storage';
import { composeWithDevTools } from 'redux-devtools-extension';
import { UserReducer } from './User';
import { ChatReducer } from './Chat';
import { RoutingReducer } from './Navigation';
import { sagaInit } from './saga';

const persistConfig = {
	key: 'root',
	storage: AsyncStorage,
	whitelist: [
		'User',
	],
}

const rootReducer = combineReducers(
	{
		Navigation: RoutingReducer,
		User: UserReducer,
		Chat: ChatReducer,
	}
);

const persistedReducer = persistReducer(persistConfig, rootReducer)

const sagaMiddleware = createSagaMiddleware();
const middlewareList = [sagaMiddleware];

export const Store = createStore(
	persistedReducer,
	composeWithDevTools(applyMiddleware(...middlewareList)),
);
export const Persistor = persistStore(Store, null, () => Store.getState())

sagaMiddleware.run(sagaInit, Store);