import socketIO from 'socket.io-client';
import { eventChannel } from 'redux-saga';
import {
	all, put, call, takeLatest, take, spawn, takeEvery,
} from 'redux-saga/effects';
import { UserActions } from './User';
import config from '../../config.json';

let socket = null;

export function* sagaInit() {
	yield all([
		_waitPersist(),
		WatchActions(),
		UserActions(),
	]);
}

export function* WatchActions() {
  	try {
		yield takeLatest('LOGOUT', () => {
			if (socket) socket.disconnect();
		})

		yield takeLatest('SOCKET:CONNECT', connect);
		yield takeEvery('CHAT:SEND', ({ params }) => {
			socket.emit('CHAT:SEND', params);
		})
		yield takeEvery('CHAT:DELETE', ({ params }) => {
			socket.emit('CHAT:DELETE', params);
		})
	} catch (error) {
		console.error('WatchAction Error: ', error);
	}
}

function* connectSocket(auth) {
	if (socket) socket.disconnect();
	socket = socketIO(config.socket+`?token=${auth}`, { secure: false, transports: ['websocket'] });
	return eventChannel((emitter) => {
		socket.on('connect', () => {
			emitter({ type: 'SOCKET:CONNECTED' });
		});

		socket.on('CHAT:HISTORY', data => {
			emitter({ type: 'CHAT:FLUSH' })
			emitter({ type: 'CHAT:MESSAGES', params: data })
		})
		socket.on('CHAT:MESSAGE', data => {
			emitter({ type: 'CHAT:MESSAGES', params: [data] })
		})
		socket.on('CHAT:ERASE', data => {
			emitter({ type: 'CHAT:ERASE', params: data });
		})

		socket.on('disconnect', () => {
			emitter({ type: 'SOCKET:DISCONNECTED' });
		});

		return () => { }
	});
}

export function* connect(auth) {
	yield spawn(_socketSaga, auth.params);
}

export function* _socketSaga(auth) {
	const channel = yield call(connectSocket, auth);
	while (true) {
		const channelAction = yield take(channel);
		yield put(channelAction);
	}
}

export function* _waitPersist() {
	try {
		yield takeLatest('persist/REHYDRATE', _InitApp)
	} catch (error) {
		console.log('persist/REHYDRATE error', error)
	}
}

export function* _InitApp({ payload }) {
	yield put({ type: 'APP_INIT' })
	const token = ((payload || {}).User || {}).Token
	if (socket) socket.disconnect();
	if (token) { yield put({ type: 'SOCKET:CONNECT', params: token }) }
}
