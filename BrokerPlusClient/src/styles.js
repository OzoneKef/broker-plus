export default {
	color: {
		dimBackground: '#20232a',
		brightBackground: '#dfdcd5',
		screenBackground: '#20232acc',
		screenBackgroundBright: '#dfdcd5cc',
		dimHeaderBackground: '#1c1e21',
		brightFont: '#fff',

		brightUnderlineAppear: '#ffff',
		brightUnderlineFade: '#fff8',

		dimUnderlineAppear: '#000f',
		dimUnderlineFade: '#0008',

		AppBG: '#888',
	},
	linkStyle: {
		color: '#61dafb',
		textDecorationLine: 'underline',
	}
}