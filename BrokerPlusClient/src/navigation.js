import React from 'react';
import AuthScreen from './screens/Auth';
import RegisterScreen from './screens/Register';
import NewsScreen from './screens/News';
import ChatScreen from './screens/Chat';
import CaseScreen from './screens/Case';
import EmailScreen from './screens/EmailVerification';
import CaseItem from './components/CaseItem';
import NewsItem from './components/NewsItem';
import { Store } from './reducers/store';

const routes = {
	Auth: {
		name: 'Авторизация',
		component: props => <AuthScreen {...props} />,
		options: {
			onBack: () => {},
			noHeader: true,
			noFooter: true,
		}
	},
	Register: {
		name: 'Регистрация',
		component: props => <RegisterScreen {...props} />,
		options: {
			onBack: () => Store.dispatch({ type: 'NAVIGATE', params: { route: 'Auth', params: {} } }),
			noHeader: true,
			noFooter: true,
		}
	},
	Email: {
		name: 'Подтверждение регистрации',
		component: props => <EmailScreen {...props} />,
		options: {
			onBack: () => Store.dispatch({ type: 'LOGOUT' }),
			noFooter: true,
		}
	},
	News: {
		name: 'Новости',
		component: props => <NewsScreen {...props} />,
	},
	Chat: {
		name: 'Чат',
		component: props => <ChatScreen {...props} />,
	},
	Case: {
		name: 'Портфель',
		component: props => <CaseScreen {...props} />,
	},
	CaseItem: {
		name: 'Назад',
		component: props => <CaseItem {...props} />,
	},
	NewsItem: {
		name: 'Назад',
		component: props => <NewsItem {...props} />,
	},
}

export default routes;