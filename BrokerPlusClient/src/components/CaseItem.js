import React from 'react';
import {
    View,
    Text,
    Image,
    FlatList,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
    Linking
} from 'react-native';
import Input from './Input';
import { connect } from 'react-redux';
import RatingIcon from '../icons/rating.png';
import testPic from '../icons/casetestpic.png';

class CaseItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        }

    }

    render() {

        return <>
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>

                <Text style={styles.name}>{this.props.data.name}</Text>

                <View style={{ flexDirection: 'row', marginLeft: 20 }}>
                    <Text style={styles.broker}>{this.props.data.broker}</Text>
                    {new Array(this.props.data.rating).fill(1).map(item => <Image style={styles.rating} source={RatingIcon} />)}
                </View>

                <Text style={{ fontWeight: 'bold', margin:5 }}>Доходность</Text>
                <Image style={styles.pic} source={testPic} />

                <Text style={styles.title}>Показатели</Text>

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ width: '50%', alignItems: 'center' }}>
                        <Text style={styles.percentGreen}>{this.props.data.percent}</Text>
                        <Text style={styles.smallGreyText}>Доходность</Text>
                    </View>

                    <View style={{ borderRightColor: '#dcdcdc', borderRightWidth: 1, height: 50, marginBottom: 20 }}></View>

                    <View style={{ width: '50%', alignItems: "center" }}>
                        <Text style={styles.percentRed}>{this.props.data.maxDrop}</Text>
                        <Text style={styles.smallGreyText}>Макс. просадка</Text>
                    </View>
                </View>

                <View style={{ borderBottomColor: '#dcdcdc', borderBottomWidth: 1, marginBottom: 20, marginLeft:10, marginRight:10,}} />

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ width: '50%', alignItems: "center" }}>
                        <Text style={styles.percentGrey}>{this.props.data.startAuctionDate}</Text>
                        <Text style={styles.smallGreyText}>Начало торгов</Text>
                    </View>

                    <View style={{ borderRightColor: '#dcdcdc', borderRightWidth: 1, height: 50, marginLeft:10, marginRight:10, }}></View>

                    <View style={{ width: '50%', alignItems: "center" }}>
                        <Text style={styles.percentGreen}>{this.props.data.ita}</Text>
                        <Text style={styles.smallGreyText}>ИТА</Text>
                    </View>
                </View>

                <Text style={styles.title}>Минимальная сумма</Text>
                <View style={{ borderBottomColor: '#dcdcdc', borderBottomWidth: 1, marginBottom: 20, marginLeft:10, marginRight:10, }} />
                <Text style={{ alignSelf: "center" }}>{this.props.data.minSumma} {'₽'}</Text>
                <View style={{ borderBottomColor: '#dcdcdc', borderBottomWidth: 1, marginBottom: 20, marginTop: 20, marginLeft:10, marginRight:10,}} />

                <Text style={{ fontWeight: 'bold', marginLeft: 5 }}>Описание</Text>
                <Text style={{ marginLeft: 4 }}> </Text>

                <Text style={styles.title}>Автоследование</Text>
                
                <TouchableOpacity onPress={() => {Linking.openURL('https://clc.to/cheb_openeg') }} style={styles.button}>
							<Text style={styles.buttonText}>Подключить</Text>
						</TouchableOpacity>
            </ScrollView>
        </>
    }
}

export default connect(
    state => ({
        Case: state.Case,
    }),
    {

    },
)(CaseItem)

const styles = StyleSheet.create({
    listContainer: {},
    listItem: {
        backgroundColor: '#fff',
        width: '100%',
        marginBottom: 10,
        padding: 15,
    },
    title: {
        alignSelf: 'center',
        marginBottom: 20, marginTop: 20,
        fontSize: 20,
    },
    name: {
        fontSize: 22,
        fontWeight: 'bold',
        margin: 20,
    },
    smallGreyText: {
        fontSize: 14,
        color: '#dcdcdc'
    },
    percentGreen: {
        alignSelf: "center",
        color: 'green'
    },
    percentRed: {
        alignSelf: "center",
        color: 'red'
    },
    percentGrey: {
        alignSelf: "center",
        color: 'grey'
    },
    pic: {
        width: '100%',
        resizeMode: 'stretch',
    },
    rating: {
        marginLeft: 2,
    },
    broker: {
        color: '#00aaff'
    },
    button:{
        backgroundColor:'#facb31',
        marginBottom:30,
        borderRadius:7,
        marginLeft:10, marginRight:10,
    },
    buttonText:{
        fontSize:18, 
        alignSelf: "center",
        paddingTop:20,
        paddingBottom:20,
    }
});