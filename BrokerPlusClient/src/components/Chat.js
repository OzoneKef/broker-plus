import React from 'react';
import {
	View,
	Text,
	Image,
	FlatList,
	TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Input from './Input';
import { connect } from 'react-redux';

import SendIcon from '../icons/send.png';

let chars = '0123456789zxcvbnmasdfghjklqwertyuiopячсмитьбюфывапролджэйцукенгшщзхъё'.split('');
for(var i = chars.length - 1; i > 0; i--) {
	var j = Math.floor(Math.random() * (i + 1));
	var tmp = chars[i];
	chars[i] = chars[j];
	chars[j] = tmp;
}

class Chat extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {
			text: null,
		}
	}

	getSeed(seed) {
		const r = 255 - chars.findIndex(item => item===seed[0])+1 % 64;
		const g = 255 - chars.findIndex(item => item===seed[1])+1 % 64;
		const b = 255 - chars.findIndex(item => item===seed[2])+1 % 64;
		return `rgb(${r}, ${g}, ${b})`;
	}	
	render() {
		const { text } = this.state;

		return <>
			<View style={{ flex: 1 }}>
				<FlatList
					contentContainerStyle={{
						justifyContent: 'flex-end',
						paddingHorizontal: 15,
						paddingTop: 15,
					}}
					inverted
					data={this.props.Chat[this.props.room]}
					renderItem={({ item, index }) => {
						const date = new Date(item.date);
						const author = `${(item.author||{}).firstname} ${(item.author||{}).lastname}`;
						const colorHash = this.getSeed(author);
						const isOwner = this.props.User.CurrentUser.id === item.author.id;
						return <View style={{
							marginBottom: 10,
							backgroundColor: isOwner ? 'white' : colorHash,
							paddingHorizontal: 10,
							paddingVertical: 2,
							borderRadius: 10,
							marginRight: isOwner ? 0 : 15,
							marginLeft: isOwner ? 15 : 0,
							elevation: 2,
							position: 'relative',
						}}>
							{this.props.User.CurrentUser.isadmin ? <Text onPress={() => this.props.deleteMessage(item.id)} style={{ top: 0, right: 7, color: 'red', position: 'absolute', transform: [ {scaleX: 1.2} ] }}>x</Text>:null}
							<Text style={{ fontSize: 12, opacity: 0.6, fontWeight: 'bold', marginBottom: -5 }}>{author}</Text>
							<Text>{item.text}</Text>
							<Text style={{ textAlign: 'right', fontSize: 10, opacity: 0.6 }}>{date.getHours().toString().padStart(2, '0')}:{date.getMinutes().toString().padStart(2, '0')}</Text>
						</View>}
					}
					keyExtractor={item => item.id}
					style={{ flex: 1 }}
				/>
				<LinearGradient colors={['#ffffff00', '#ffffffff', '#ffffffff']} style={{ paddingTop: 30, marginTop: -30 }} locations={[0, 0.35 , 1]}>
					<View style={{ flexDirection: 'row', alignItems: 'center', flex: 0, padding: 5,marginBottom:15, borderWidth: 1, margin: 5, borderRadius: 10 }}>
						<Input
							onChange={text => this.setState({ text })}
							value={text}
							placeholder={'Сообщение'}
							theme={'light'}
							style={{ flex: 1 }}
						/>
						<TouchableOpacity onPress={() => {
							if (this.state.text) {
								this.props.Send({ room: this.props.room, text: this.state.text })}
								this.setState({ text: null })
							}
						}>
							<Image
								source={SendIcon}
								style={{
									height: 32,
									width: 32,
								}}
							/>
						</TouchableOpacity>
					</View>
				</LinearGradient>
			</View>
		</>
	}
}

export default connect(
	state => ({
		Chat: state.Chat,
		User: state.User,
	}),
	{
		Send: params => ({ type: 'CHAT:SEND', params }),
		deleteMessage: params => ({ type: 'CHAT:DELETE', params }),
	},
)(Chat)