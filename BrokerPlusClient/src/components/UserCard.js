import React from 'react';
import {
	View,
	Text
} from 'react-native';
import { connect } from 'react-redux';

class UserCard extends React.Component {
	render() {
		return <>
			<View>
				<Text>{this.props.User.firstname}</Text>
			</View>
		</>
	}
}

export default connect(
	state => ({
		User: state.User.CurrentUser,
	}),
	{},
)(UserCard)