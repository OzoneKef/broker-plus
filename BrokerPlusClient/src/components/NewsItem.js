import React from 'react';
import {
    View,
    Text,
    Image,
    FlatList,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
} from 'react-native';
import Input from './Input';
import { connect } from 'react-redux';
import RatingIcon from '../icons/rating.png';
import testPic from '../icons/casetestpic.png';

class NewsItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        }

    }

    render() {
        return <>
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                <Text style={styles.title}>{this.props.data.title}</Text>
                <Image style={styles.pic} source={this.props.data.pic} />
                <Text style={styles.date}>{this.props.data.date}</Text>
                <Text style={styles.text}>{this.props.data.text}</Text>
            </ScrollView>
        </>
    }
}

export default connect(
    state => ({
        News: state.News,
    }),
    {

    },
)(NewsItem)

const styles = {
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        margin:10
    },
    date: {
        color:'grey',
        margin:10
    },
    pic: {
        width: '100%',
        height: 150,
    },
    text: {
        fontSize:16,
        margin:10
    }
};