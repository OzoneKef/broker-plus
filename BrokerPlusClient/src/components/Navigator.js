import React from 'react';
import {
	View,
	Text,
	Animated,
	Easing,
	BackHandler,
	TouchableWithoutFeedback,
	Dimensions,
	Keyboard,
	Image,
	TouchableOpacity,
	ScrollView,
	Linking
} from 'react-native';
import { connect } from 'react-redux';
import Routing from '../navigation';

import GlobalStyles from '../styles';
import {
	StatusBar,
} from 'react-native';

import BackArrow from '../icons/back.png';
import AppBackground from '../icons/appbackground.png';

import CaseIcon from '../icons/case.png';
import ChatIcon from '../icons/chat.png';
import NewsIcon from '../icons/news.png';
import MenuIcon from '../icons/menu.png';
import ExitIcon from '../icons/exit.png';
import MenuBack from '../icons/menubackground.png'
import UserIcon from '../icons/user.png'

class Screen extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			transition: new Animated.Value(0),
		}
	}

	componentDidMount() {
		Animated.timing(this.state.transition, {
			toValue: 1,
			duration: 500,
			easing: Easing.quad,
			useNativeDriver: false,
		}).start();
	}

	componentDidUpdate(prevProps) {
		if (prevProps.transition !== this.props.transition) {
			Animated.timing(this.state.transition, {
				toValue: 2,
				duration: 500,
				easing: Easing.quad,
				useNativeDriver: true,
			}).start();
		}
	}

	render() {
		return <>
			<Animated.View
				style={{
					position: 'absolute',
					top: 0,
					left: 0,
					transform: [
						{ perspective: Dimensions.get('window').width },
						{
							translateX: this.state.transition.interpolate({
								inputRange: [0, 1, 2],
								outputRange: [Dimensions.get('window').width, 0, -Dimensions.get('window').width],
							})
						},
						{
							rotateY: this.state.transition.interpolate({
								inputRange: [0, 1, 2],
								outputRange: ['-90deg', '0deg', '90deg'],
							})
						},
					],
					width: 0,
					bottom: 0,
					width: '100%',
					zIndex: 1,
					...this.props.style,
				}}
			>
				{this.props.component}
			</Animated.View>
		</>
	}
}

class Navigator extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			transition: false,
			routes: [
				{ key: props.Navigation.Update, Route: props.Navigation.Route, Params: props.Navigation.Params },
			],
			height: new Animated.Value(Dimensions.get('window').height),

			header: new Animated.Value(0),
			footer: new Animated.Value(0),

			menu: false,
			menuPos: new Animated.Value(0),

			showHeader: false,
			showFooter: false,
		}
	}

	componentDidMount() {
		BackHandler.addEventListener('hardwareBackPress', this.onBack);
		Keyboard.addListener('keyboardDidShow', this.handleKeyboard);
		Keyboard.addListener('keyboardDidHide', this.handleKeyboard);
		setTimeout(() => {
			StatusBar.setBarStyle('light-content');
			StatusBar.setBackgroundColor(GlobalStyles.color.dimHeaderBackground);
		}, 1000);
		if ((Routing[this.props.Navigation.Route].options || {}).noHeader) {
			Animated.timing(this.state.header, {
				toValue: 0,
				duration: 500,
				easing: Easing.quad,
				useNativeDriver: true,
			}).start();
		} else {
			Animated.timing(this.state.header, {
				toValue: 1,
				duration: 500,
				easing: Easing.quad,
				useNativeDriver: true,
			}).start();
		}
		if ((Routing[this.props.Navigation.Route].options || {}).noFooter) {
			Animated.timing(this.state.footer, {
				toValue: 0,
				duration: 500,
				easing: Easing.quad,
				useNativeDriver: true,
			}).start();
		} else {
			Animated.timing(this.state.footer, {
				toValue: 1,
				duration: 500,
				easing: Easing.quad,
				useNativeDriver: true,
			}).start();
		}
	}

	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.onBack)
		Keyboard.removeListener('keyboardDidShow', this.handleKeyboard);
		Keyboard.removeListener('keyboardDidHide', this.handleKeyboard);
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevState.menu !== this.state.menu) {
			if (this.state.menu) {
				Animated.timing(this.state.menuPos, {
					toValue: 1,
					duration: 500,
					easing: Easing.sin,
					useNativeDriver: true,
				}).start();
			} else {
				Animated.timing(this.state.menuPos, {
					toValue: 0,
					duration: 500,
					easing: Easing.sin,
					useNativeDriver: true,
				}).start();
			}
		}
		if (prevProps.Navigation.Update !== this.props.Navigation.Update) {
			const newroutes = [...this.state.routes];
			newroutes.push({
				key: this.props.Navigation.Update,
				Route: this.props.Navigation.Route,
				Params: this.props.Navigation.Params,
			})
			if ((Routing[this.props.Navigation.Route].options || {}).noHeader) {
				Animated.timing(this.state.header, {
					toValue: 0,
					duration: 500,
					easing: Easing.quad,
					useNativeDriver: true,
				}).start();
			} else {
				Animated.timing(this.state.header, {
					toValue: 1,
					duration: 500,
					easing: Easing.quad,
					useNativeDriver: true,
				}).start();
			}
			if ((Routing[this.props.Navigation.Route].options || {}).noFooter) {
				Animated.timing(this.state.footer, {
					toValue: 0,
					duration: 500,
					easing: Easing.quad,
					useNativeDriver: true,
				}).start();
			} else {
				Animated.timing(this.state.footer, {
					toValue: 1,
					duration: 500,
					easing: Easing.quad,
					useNativeDriver: true,
				}).start();
			}
			this.setState({
				transition: true,
				routes: newroutes,
			}, () => setTimeout(() => {
				const newroutes = [...this.state.routes];
				newroutes.shift()
				this.setState({
					transition: false,
					routes: newroutes,
				})
			}, 500));
		}
	}

	handleKeyboard = (e) => {
		Animated.timing(this.state.height, {
			toValue: Dimensions.get('window').height - e.endCoordinates.height,
			duration: 200,
			easing: Easing.linear,
			useNativeDriver: false,
		}).start();
	}

	onBack = () => {
		if (!this.state.transition) {
			if (((Routing[this.state.routes[0].Route] || {}).options || {}).onBack) {
				Routing[this.state.routes[0].Route].options.onBack();
			} else if (this.props.Navigation.history.length) {
				this.props.goBack();
			}
		}
		return true;
	}

	render() {
		return <>
			<View style={{ flex: 1, backgroundColor: Routing[this.state.routes[0].Route].bg || GlobalStyles.color.AppBG, position: 'relative' }}>
				<Image
					resizeMode='cover'
					source={AppBackground}
					style={{
						position: 'absolute',
						top: 0,
						bottom: 0,
						left: 0,
						right: 0,
					}}
				/>
				<Animated.View
					style={{
						height: this.state.height,
						position: 'relative',
					}}>
					<Animated.View
						style={{
							position: 'absolute',
							top: 0,
							height: 50,
							left: 0,
							right: 0,
							backgroundColor: GlobalStyles.color.dimBackground,
							zIndex: 2,
							flexDirection: 'row',
							alignItems: 'center',
							justifyContent: 'flex-start',
							transform: [
								{
									translateY: this.state.header.interpolate({
										inputRange: [0, 1],
										outputRange: [-50, 0],
									})
								},
							]
						}}
					>
						<TouchableOpacity onPress={this.onBack}>
							<Image
								source={BackArrow}
								style={{
									width: 40,
									height: 40,
								}}
							/>
						</TouchableOpacity>
						<Text style={{
							color: GlobalStyles.color.brightFont,
							fontSize: 20,
						}}>{Routing[this.props.Navigation.Route].name}</Text>
					</Animated.View>
					{this.state.routes.map((screen, index) => <Screen
						style={{ paddingTop: (Routing[screen.Route].options || {}).noHeader ? 0 : 50, paddingBottom: (Routing[screen.Route].options || {}).noFooter ? 0 : 80 }}
						transition={this.state.transition && !index} key={screen.key} component={Routing[screen.Route].component(screen.Params)} />)}
					{this.state.transition && <TouchableWithoutFeedback><View style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0 }} /></TouchableWithoutFeedback>}
					<Animated.View
						style={{
							position: 'absolute',
							bottom: 0,
							height: 80,
							left: 0,
							right: 0,
							backgroundColor: GlobalStyles.color.dimBackground,
							zIndex: 2,
							transform: [
								{
									translateY: this.state.footer.interpolate({
										inputRange: [0, 1],
										outputRange: [80, 0],
									})
								},
							],
							flexDirection: 'row',
							alignItems: 'flex-start',
							justifyContent: 'space-between',
							paddingTop: 10,
							marginBottom: 10,
							paddingHorizontal: 20,
						}}
					>
						<TouchableOpacity style={MenuItemStyle} onPress={() => this.props.navigateTo('News')}>
							<View style={MenuItemView}>
								<Image source={NewsIcon} />
								<Text style={MenuItemStyleText}>Новости</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity style={MenuItemStyle} onPress={() => this.props.navigateTo('Chat')}>
							<View style={MenuItemView}>
								<Image source={ChatIcon} />
								<Text style={MenuItemStyleText}>Чат</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity style={MenuItemStyle} onPress={() => this.props.navigateTo('Case')}>
							<View style={MenuItemView}>
								<Image source={CaseIcon} />
								<Text style={MenuItemStyleText}>Портфель</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity style={MenuItemStyle} onPress={() => this.setState({ menu: true })}>
							<View style={MenuItemView}>
								<Image source={MenuIcon} />
								<Text style={MenuItemStyleText}>Меню</Text>
							</View>
						</TouchableOpacity>
					</Animated.View>
				</Animated.View>

				<Animated.View style={{
					zIndex: 4,
					position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,
					backgroundColor: GlobalStyles.color.dimBackground,
					opacity: this.state.menuPos.interpolate({
						inputRange: [0, 1],
						outputRange: [0, 0.6],
					}),
					transform: [
						{
							translateX: this.state.menuPos.interpolate({
								inputRange: [0, 1],
								outputRange: [-Dimensions.get('window').width, 0],
							})
						},
					],
				}}>
					<TouchableOpacity style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, zIndex: 10 }} onPress={() => this.setState({ menu: false })}>
						<View style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0 }} />
					</TouchableOpacity>
				</Animated.View>
				<Animated.View style={{
					zIndex: 5,
					backgroundColor: GlobalStyles.color.brightBackground,
					position: 'absolute', top: 0, bottom: 0, left: 0, right: Dimensions.get('window').width / 3,
					transform: [
						{
							translateX: this.state.menuPos.interpolate({
								inputRange: [0, 1],
								outputRange: [-Dimensions.get('window').width * 4, 0],
							})
						},
					],
				}}>
					<ScrollView >
						<View style={{ position: 'relative' }}>
							<Image source={MenuBack} style={{ height: 140, resizeMode: 'stretch', zIndex: 6 }} />
							<View style={{ position: 'absolute', zIndex: 7, flexDirection: 'row', top: 70, left: 20 }}>
								<Image source={UserIcon} style={{}} />
								<Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 16, marginLeft: 10, marginTop: 5 }}>{this.props.User.CurrentUser.firstname} {this.props.User.CurrentUser.lastname}</Text>
							</View>
						</View>

						<View style={{ padding: 15 }}>
							<View style={SideMenuLine} />
							<TouchableOpacity onPress={() => this.setState({ menu: false }, () => this.props.navigateTo('News'))} style={SideMenuItem}>
								<Text style={SideMenuItemText}>Новости</Text>
							</TouchableOpacity>
							<View style={SideMenuLine} />

							<TouchableOpacity onPress={() => this.setState({ menu: false }, () => this.props.navigateTo('Chat'))} style={SideMenuItem}>
								<Text style={SideMenuItemText}>Чат</Text>
							</TouchableOpacity>
							<View style={SideMenuLine} />

							<TouchableOpacity onPress={() => this.setState({ menu: false }, () => this.props.navigateTo('Case'))} style={SideMenuItem}>
								<Text style={SideMenuItemText}>Портфель</Text>
							</TouchableOpacity>
							<View style={SideMenuLine} />

							<Text style={{ color: 'grey', fontStyle: 'italic', marginTop: 30 }}>Контакты: </Text>
							<TouchableOpacity onPress={() => { Linking.openURL('https://clc.to/cheb_openeg') }} style={SideMenuButton}>
								<Text style={SideMenuButtonText}>Открыть счет </Text>
							</TouchableOpacity>

							<Text style={{ color: 'grey', fontStyle: 'italic' }}>Viber: </Text>
							<TouchableOpacity onPress={() => { Linking.openURL('') }} style={SideMenuItem}>
								<Text style={SideMenuItemText}>+7 (995) 913-45-32 -></Text>
							</TouchableOpacity>
							<Text style={{ color: 'grey', fontStyle: 'italic' }}>Whats'app: </Text>
							<TouchableOpacity onPress={() => { Linking.openURL('https://wa.me/+79959134532') }} style={SideMenuItem}>
								<Text style={SideMenuItemText}>+7 (995) 913-45-32 -></Text>
							</TouchableOpacity>
							<Text style={{ color: 'grey', fontStyle: 'italic' }}>instagram: </Text>
							<TouchableOpacity onPress={() => { Linking.openURL('https://www.instagram.com/bussines.one') }} style={SideMenuItem}>
								<Text style={SideMenuItemText}> @bussines.one -></Text>
							</TouchableOpacity>

							<View style={SideMenuLine} />
							<TouchableOpacity onPress={() => this.setState({ menu: false }, () => this.props.logout({ menu: false }))} style={SideMenuItem}>
								<Image source={ExitIcon} style={{ marginTop: 4, marginLeft: 4 }} tintColor='grey' />
								<Text style={SideMenuItemText}>Выйти</Text>
							</TouchableOpacity>
						</View>
					</ScrollView>
				</Animated.View>
			</View>
		</>
	}
}

const MenuItemStyle = {}

const SideMenuLine = {
	borderBottomColor: '#d1d1d1', borderBottomWidth: 1
}

const SideMenuItem = {
	backgroundColor: '#fff',
	flexDirection: 'row',
	borderRadius: 7,
	marginBottom: 5,
	marginTop: 5,
	marginLeft: 10, marginRight: 10, marginBottom: 10, marginTop: 10,
}

const SideMenuItemText = {
	fontSize: 16,
	padding: 10,
}

const SideMenuButton = {
	backgroundColor: '#facb31',
	borderRadius: 7,
	marginLeft: 10, marginRight: 10, marginBottom: 10, marginTop: 10,
}

const SideMenuButtonText = {
	color: '#fff',
	fontSize: 18,
	alignSelf: "center",
	paddingTop: 10,
	paddingBottom: 10,
}

const MenuItemStyleText = {
	fontSize: 11,
	color: 'white',
}

const MenuItemView = {
	alignItems: 'center',
	flexDirection: 'column'
}

export default connect(
	state => ({
		Navigation: state.Navigation,
		User: state.User,
	}),
	{
		navigateTo: route => ({ type: 'NAVIGATE', params: { route, params: {} } }),
		logout: () => ({ type: 'LOGOUT' }),
		goBack: () => ({ type: 'HISTORY_POP' }),
	},
)(Navigator)