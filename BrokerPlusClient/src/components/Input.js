import React from 'react';
import {
	View,
	TextInput,
	Animated,
	Easing,
	TouchableOpacity,
	Keyboard,
} from 'react-native';
import GlobalStyles from '../styles';
import Cross from '../icons/cross.png';

export default class Input extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			active: false,
			animation: new Animated.Value(0),

			panHandler: 'panHandler',
			keyBoardNeedFix: false,
		}
	}

	componentDidMount() {
		Keyboard.addListener('keyboardDidShow', this.handleKeyboard);
    	Keyboard.addListener('keyboardDidHide', this.handleKeyboard);
	}

	componentWillUnmount() {
		Keyboard.removeListener('keyboardDidShow', this.handleKeyboard);
    	Keyboard.removeListener('keyboardDidHide', this.handleKeyboard);
	}

	handleKeyboard = () => {
		setTimeout(() => {
			this.setState({ keyBoardNeedFix: true }, () => this.setState({ keyBoardNeedFix: false }))
		}, 100)
	}

	componentDidUpdate(prevState) {
		if (prevState.active !== this.state.active) {
			if (this.state.active) {
				this.Focus();
			} else {
				this.Blur();
			}
		}
	}

	Focus = () => {
		Animated.timing(this.state.animation, {
			toValue: 1,
			duration: 1000,
			easing: Easing.out(Easing.ease),
			useNativeDriver: false,
		}).start();
	}
	Blur = () => {
		Animated.timing(this.state.animation, {
			toValue: 0,
			duration: 1000,
			easing: Easing.out(Easing.ease),
			useNativeDriver: false,
		}).start();
	}

	render() {
		const { active, keyBoardNeedFix, panHandler } = this.state;
		const { theme = 'dark', textStyle = {} } = this.props;

		return <>
			<View style={{
				position: 'relative',
				justifyContent: 'center',
				...this.props.style,
			}}>
				<TouchableOpacity
					style={{
						position: 'absolute',
						right: 0,
						width: 16,
						height: 16,
						zIndex: 3,
						justifyContent: 'center',
						alignItems: 'center',
					}}
					hitSlop={{
						top: 5,
						bottom: 5,
						left: 5,
						right: 5,
					}}
					onPress={() => this.props.onChange()}
				>
					<Animated.Image
						source={Cross}
						style={{
							height: this.state.animation.interpolate({
								inputRange: [0, 1],
								outputRange: [0, 16],
							}),
							width: this.state.animation.interpolate({
								inputRange: [0, 1],
								outputRange: [0, 16],
							}),
						}}
					/>
				</TouchableOpacity>
				<TextInput
					onChangeText={text => this.props.onChange(text)}
					value={keyBoardNeedFix ? !this.props.value ? panHandler : '' : this.props.value}
					onFocus={() => this.setState({ active: true })}
					onBlur={() => this.setState({ active: false })}
					placeholder={this.props.placeholder}
					secureTextEntry={this.props.secure}
					keyboardType={this.props.keyboardType}
					maxLength={this.props.maxLength}
					placeholderTextColor={(() => {
						switch(theme) {
							case 'light':
								return GlobalStyles.color.dimUnderlineFade;
							case 'dark':
							default: 
								return GlobalStyles.color.brightUnderlineFade;
						}
					})()}
					style={{
						fontSize: 15,
						padding: 0,
						color: (() => {
							switch(theme) {
								case 'light':
									return GlobalStyles.color.dimUnderlineAppear;
								case 'dark':
								default: 
									return GlobalStyles.color.brightUnderlineAppear;
							}
						})(),
						...textStyle,
					}}
				/>
				<View style={{ height: 1, position: 'relative', flexDirection: 'row', justifyContent: 'center' }}>
					<View style={{
						position: 'absolute',
						zIndex: 1,
						top: 0, bottom: 0,
						left: 0, right: 0,
						backgroundColor: (() => {
							switch(theme) {
								case 'light':
									return GlobalStyles.color.dimUnderlineFade;
								case 'dark':
								default: 
									return GlobalStyles.color.brightUnderlineFade;
							}
						})(),
					}}/>
					<Animated.View style={{
						position: 'relative',
						zIndex: 2,
						flexBasis: this.state.animation.interpolate({
							inputRange: [0, 1],
							outputRange: ['0%', '100%'],
						}),
						height: 1,
						backgroundColor: (() => {
							switch(theme) {
								case 'light':
									return GlobalStyles.color.dimUnderlineAppear;
								case 'dark':
								default: 
									return GlobalStyles.color.brightUnderlineAppear;
							}
						})(),
					}}/>
				</View>
			</View>
		</>
	}
}