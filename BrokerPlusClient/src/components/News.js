import React from 'react';
import {
    View,
    Text,
    Image,
    FlatList,
    TouchableOpacity,
    StyleSheet,
    ScrollView
} from 'react-native';
import Input from './Input';
import { connect } from 'react-redux';

import SendIcon from '../icons/send.png';
import news1 from '../icons/news1.png';
import news2 from '../icons/news2.png';
import news3 from '../icons/news3.png';

class News extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

            data: [
                {
                    id: 0,
                    title: 'Риски кризиса неплатежей растут',
                    date: '26.04.2020 Воскресенье 16.57',
                    pic: news1,
                    text: 'Больше трети компаний почти из всех отраслей рискуют обанкротиться, показал мониторинг Центра стратегических разработок (ЦСР) 250 компаний (проводился с 20 по 26 апреля). В самом тяжелом состоянии компании из сферы торговли и услуг, высок риск банкротства также у легкой и пищевой промышленности. Но ключевой риск – банкротство контрагентов компаний (о нем сообщили 94% опрошенных предпринимателей). Это может привести к эффекту домино, когда экономически связанные компании банкротятся по цепочке, предупреждают эксперты ЦСР.',

                },
                {
                    id: 1,
                    title: 'Минфин объявил о продаже валюты из ФНБ в апреле',
                    date: '26.04.2020 Среда 16.00',
                    pic: news2,
                    text: 'Продажи иностранной валюты Минфином в предстоящий месяц, с 7 апреля по 12 мая, составят в рублевом эквиваленте 77,8 млрд руб., то есть около $1 млрд, сообщил Минфин. Недополученные нефтегазовые доходы федерального бюджета от цен на нефть ниже базовой ($42,4 за баррель Urals) оцениваются на ближайший месяц в 55,8 млрд руб. С учетом корректировки на уточненный объем нефтегазовых доходов в марте (минус 20 млрд руб.) запланированный объем продаж валюты на открытом рынке составит 77,8 млрд, или 3,5 млрд руб. ежедневно. Для сравнения, Банк России сейчас тратит ежедневно по 16 млрд руб., продавая валюту на рынке.',
                },
                {
                    id: 2,
                    title: 'Сбербанк повысил прогноз курса доллара на 6 руб',
                    date: '26.04.2020 Понедельник 11.51',
                    pic: news3,
                    text: 'Сбербанк повысил более чем на 6 руб. курс доллара США, заложенный в его макроэкономический прогноз на 2020 год, следует из презентации итогов работы банка по МСФО за первый квартал 2020 года. В соответствии с документом по итогам 2020 года средний курс составит 70,7 руб., а в феврале Сбербанк прогнозировал его на уровне 64,5 руб. Прогноз средних курсов доллара на 2021 и 2022 годы Сбербанк повысил на 5 руб. — с 65 руб. до 70 руб. и с 65,5 руб. до 70,5 руб. соответственно. Мировые цены на российскую нефть марки Urals, согласно прогнозу Сбербанка, составят $32 за баррель в 2020 году и $50–55 в 2021–2022 годах. В феврале банк прогнозировал цену $59 за баррель Urals в 2020 году и $62 в следующие два года.',
                }
            ]
        }

    }

    render() {

        return <>
            <View style={{ flex: 1 }}>
                <FlatList
                    contentContainerStyle={{
                        justifyContent: 'flex-start',
                    }}
                    data={this.state.data.reverse()}
                    renderItem={({ item }) =>
                        <TouchableOpacity onPress={() => { this.props.openNewsItem({ type: 'NewsItem', data: item }) }}>
                            <View style={styles.listItem}>
                                <Image style={styles.pic} source={item.pic} />
                                <Text style={styles.title}>{item.title}</Text>
                                <Text style={styles.date}>{item.date}</Text>
                            </View>
                        </TouchableOpacity>
                    }
                    keyExtractor={item => item.id}
                    style={{}}
                />

            </View>
        </>
    }
}

export default connect(
    state => ({
        News: state.News,
    }),
    {
        openNewsItem: params => ({ type: 'NAVIGATE', params: { route: 'NewsItem', params } }),
    },
)(News)

const styles = {
    listContainer: {},
    listItem: {
        backgroundColor: '#fff',
        width: '100%',
        marginBottom: 10,
        padding: 15,
    },
    broker: {
        color: '#00aaff'
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    text: {
        color: 'grey'
    },
    date: {


    },
    pic: {
        width: '100%',
        height: 150

    }

};