import React from 'react';
import {
    View,
    Text,
    Image,
    FlatList,
    TouchableOpacity,
    StyleSheet,
} from 'react-native';
import Input from './Input';
import { connect } from 'react-redux';

import SendIcon from '../icons/send.png';
import RatingIcon from '../icons/rating.png';

class CaseList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

            data: [
                {
                    id: 0,
                    broker: "AVAKS",
                    name: "Понятные правила",
                    rating: 2,
                    percent: "+498%",
                    maxDrop: "−19.75%",
                    startAuctionDate: "11.01.2020",
                    ita:"0.23",
                    minSumma:"200 000 ₽",
                    info: "Подписчики 323, действует 11 мес., еженедельные сделки, от 150 000 ₽, риск Агрессивный",
                    fullinfo: "",
                    pic: "",

                },
                {
                    id: 1,
                    broker: "Росток",
                    name: "02.2020",
                    rating: 0,
                    percent: "+498%",
                    maxDrop: "−19.75%",
                    startAuctionDate: "11.01.2020",
                    ita:"0.23",
                    minSumma:"200 000 ₽",
                    info: "4, действует 3 мес., ежедневные сделки, от 250 000 ₽, риск Агрессивный",
                    fullinfo: "",
                    pic: ""
                },
                {
                    id: 2,
                    broker: "Invest Consulting",
                    name: "Trend INVEST",
                    rating: 3,
                    percent: "+498%",
                    maxDrop: "−19.75%",
                    startAuctionDate: "11.01.2020",
                    ita:"0.23",
                    minSumma:"200 000 ₽",
                    info: "15, действует 9 мес., ежедневные сделки, от 100 000 ₽, риск Агрессивный",
                    fullinfo: "",
                    pic: ""
                }
            ]
        }

    }

    render() {

        return <>
            <View style={{ flex: 1 }}>
                <FlatList
                    contentContainerStyle={{
                        justifyContent: 'flex-start',
                        flex: 1,
                    }}
                    data={this.state.data}
                    renderItem={({ item }) =>
                        <TouchableOpacity onPress={() => this.props.openCaseItem({type:'CaseItem',data:item})}>
                            <View style={lists.listItem}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={lists.broker}>{item.broker}</Text>
                                        {new Array(item.rating).fill(1).map(item => <Image style={lists.rating} source={RatingIcon} />)}
                                    </View>
                                    <Text style={lists.percent}>{item.percent}</Text>
                                </View>

                                <Text style={lists.name}>{item.name}</Text>
                                <Text style={lists.info}>{item.info}</Text>
                            </View>
                        </TouchableOpacity>
                    }
                    keyExtractor={item => item.id}
                    style={{ flex: 1 }}
                />

            </View>
        </>
    }
}

export default connect(
    state => ({
        Case: state.Case,
    }),
    {
        openCaseItem: params => ({ type: 'NAVIGATE', params: {  route: 'CaseItem', params } }),
    },
)(CaseList)

const lists = StyleSheet.create({
    listContainer: {},
    listItem: {
        backgroundColor: '#fff',
        width: '100%',
        marginBottom: 10,
        padding: 15,
    },
    broker: {
        color: '#00aaff'
    },
    name: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    info: {
        color: 'grey'
    },
    percent: {
        fontSize: 18,
        color: 'green'
    },
    rating: {
        marginLeft: 2,
    }

});