import React from 'react';
import {
	Animated,
	Easing,
	TouchableHighlight,
	ActivityIndicator,
} from 'react-native';
import GlobalStyles from '../styles';

export default class Input extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			animation: new Animated.Value(1),
		}
	}

	componentDidMount() {
		if (this.props.disabled) {
			this.setState({ animation: new Animated.Value(0.5) })
		}
	}

	componentDidUpdate(prevState, prevProps) {
		if (prevProps.disabled !== this.props.disabled) {
			if (this.props.disabled) {
				this.Disable();
			} else {
				this.Enable();
			}
		}
	}

	Disable = () => {
		Animated.timing(this.state.animation, {
			toValue: 0.5,
			duration: 500,
			easing: Easing.out(Easing.ease),
			useNativeDriver: false,
		}).start();
	}
	Enable = () => {
		Animated.timing(this.state.animation, {
			toValue: 1,
			duration: 500,
			easing: Easing.out(Easing.ease),
			useNativeDriver: false,
		}).start();
	}

	render() {
		const { theme = 'dark' } = this.props;

		return <>
			<TouchableHighlight
				onPress={(this.props.loading || this.props.disabled) ? () => {} : this.props.onPress}
				style={{
					position: 'relative',
					height: 40,
					...this.props.style,
				}}
			>
				<Animated.View
					style={{
						height: '100%',
						width: '100%',
						justifyContent: 'center',
						alignItems: 'center',
						opacity: this.state.animation,
						backgroundColor: (() => {
							switch(theme) {
								case 'light':
									return GlobalStyles.color.dimUnderlineFade;
								case 'dark':
								default: 
									return GlobalStyles.color.brightUnderlineFade;
							}
						})(),
					}}
				>
					{this.props.loading && <ActivityIndicator size='small' color={(() => {
							switch(theme) {
								case 'light':
									return GlobalStyles.color.dimUnderlineAppear;
								case 'dark':
								default: 
									return GlobalStyles.color.brightUnderlineAppear;
							}
						})()}/>}
					{!this.props.loading && (() => {
						if (React.isValidElement(this.props.children)) {
							return React.cloneElement(this.props.children,
								{
									style: {
										color: (() => {
											if (this.props.disabled) {
												switch(theme) {
													case 'light':
														return GlobalStyles.color.brightUnderlineAppear;
													case 'dark':
													default: 
														return GlobalStyles.color.dimUnderlineAppear;
												}
											} else {
												switch(theme) {
													case 'light':
														return GlobalStyles.color.dimUnderlineAppear;
													case 'dark':
													default: 
														return GlobalStyles.color.brightUnderlineAppear;
												}
											}
										})(),
									}
								}
							)
						} else {
							return this.props.children
						}
					})()}
				</Animated.View>
			</TouchableHighlight>
		</>
	}
}